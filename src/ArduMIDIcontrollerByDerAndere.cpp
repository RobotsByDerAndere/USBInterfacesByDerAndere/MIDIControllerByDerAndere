/**
 * @title: ArduMIDIcontrollerByDerAndere.cpp
 * @version: 4.0
 * @author: DerAndere
 * @created: 2018
 * SPDX-License-Identifier: MIT
 * Copyright DerAndere 2018
 * @license: MIT.
 * @info: https://it-by-derandere.blogspot.com/programming
 * @language: C++
 * @description: Sketch for an Arduino-compatible development
 * board that is used inside the DIY-MIDI-controller by DerAndere.
 */

#include "ArduMIDIcontrollerByDerAndere.h"
#include "Arduino.h"
#include "MIDIbyDerAndere/MIDIbyDerAndere.h"
#include "map2.h"  // NuMap (https://gitlab.com/RobotsByDerAndere/LibrariesByDerAndereForDevelopmentboards/NuMap)

// Constructor definition with list initialization: Class member initialization with syntax "class-or-identifier brace-init-list" initializes the base or member 
// named by class-or-identifier using list-initialization (which becomes value-initialization if the list is empty and aggregate-initialization when initializing an aggregate
ArduinoMIDIcontroller::ArduinoMIDIcontroller() : m_controlChange { 176 }, m_midiCCselects {22, 23, 24, 25, 26, 27}, m_potVals { 0 },  m_controllerVals { 0 }, m_controllerValsPrevious { 0 } {
}

/**
 * Read potentiometers by measuring the potential between the analog input pins and
 * common (GND), smooth potentiometer value using exponential moving average as
 * lowpass filter, map to MIDI controller values (0-127) using the function map2() of
 * the library NuMap (https://gitlab.com/RobotsByDerAndere/LibrariesByDerAndereForDevelopmentboards/NuMap)
 * and send changed controller values as MIDI message via USB
 * serial connection. Use the freeware virtual MIDI device LoopBe1
 * (http://www.nerds.de/en/loopbe1.html) as the MIDI-serial bridge.
 * @param: int8_t potIndex
 * @param: uint8_t emaFilterWeight: FilterWeight for the moving average filter
 * (default: 2)
 * @returns: void (sends MIDI controller values as uint8_t (0-127).
 * @fn: Read potentiometers by measuring the potential between the analog input pins
 * and common (GND), smooth potentiometer value using exponential moving average as
 * lowpass filter, map to MIDI controller values (0-127) using the function map2() of
 * the library NuMap (https://gitlab.com/RobotsByDerAndere/LibrariesByDerAndereForDevelopmentboards/NuMap)
 * and send changed controller values as MIDI message via USB
 * serial connection.
 */
void ArduinoMIDIcontroller::sendChangedControllerMIDI(int8_t potIndex, uint8_t emaFilterWeight){
	m_potVals[potIndex] = m_MIDIcontrollerArduinoUno.smoothReadInput(potIndex, emaFilterWeight);
	m_controllerVals[potIndex] = map2::map2(m_potVals[potIndex], 0, 1023, 0, 127);
	if (abs(m_controllerVals[potIndex] - m_controllerValsPrevious[potIndex])
			>= 1) {
		sendMIDI(m_controlChange, m_midiCCselects[potIndex],
				m_controllerVals[potIndex]);
	}
	m_controllerValsPrevious[potIndex] = m_controllerVals[potIndex];
}

int8_t ArduinoMIDIcontroller::getPotCount() {
	return m_potCount;
}


namespace {
	ArduinoMIDIcontroller ArduinoMIDIcontroller1;
}


//The setup function is called once at startup of the sketch
void setup()
{
	Serial.begin(9600);
	for (int8_t i = 0; i < ArduinoMIDIcontroller1.getPotCount(); ++i) {
		ArduinoMIDIcontroller1.attachIO(i, i, false, true);
	}
}


// The loop function is called in an endless loop
void loop()
{

/**
 * sequentially read the electric potential at each analog pin to which potis are
 * connected, smooth the values with a smoothing factor of 25 % (smoothingFactor = 25
 * (default)) and store them in the vector potiVal. Map these values to MIDI-
 * controller values in the range of 0-127 using the map2() function from the NewMap
 * library (https://gitlab.com/RobotsByDerAndere/LibrariesByDerAndereForDevelopmentboards/NuMap) 
 * (which replaces the strange standard map() function from the Arduino.h
 * library). If values changed since the last read, send MIDI via the serial port.
 */
	static uint32_t midiSendTimePrevious;

	for(int8_t i=0; i<ArduinoMIDIcontroller1.getPotCount(); ++i) {
		static uint32_t midiSendTime = millis();
		if ((midiSendTime - midiSendTimePrevious) >= (uint32_t)1) {
			ArduinoMIDIcontroller1.sendChangedControllerMIDI(i, 2);
			midiSendTimePrevious = midiSendTime;
		}
	}
}

