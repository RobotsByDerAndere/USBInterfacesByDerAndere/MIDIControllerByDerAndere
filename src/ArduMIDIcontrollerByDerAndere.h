/**
 * @title: ArduMIDIcontrollerByDerAndere.h
 * @version: 4.0
 * @author: DerAndere
 * @created: 2018
 * SPDX-License-Identifier: MIT
 * Copyright DerAndere 2018
 * @license: MIT
 * @info: https://it-by-derandere.blogspot.com/programming
 * @language: C++
 * @about: Sketch for an Arduino-compatible development
 * board that is used inside the DIY-MIDI-controller by DerAndere.
 */

#ifndef ArduMIDIcontrollerByDerAndere_H
#define ArduMIDIcontrollerByDerAndere_H
#include "Arduino.h"
#include "ArdUnoRev3API1.h"  // ArduUnoRev3API1 (https://gitlab.com/RobotsByDerAndere/LibrariesByDerAndereForDevelopmentboards/ArduUnoRev3API1)


class ArduinoMIDIcontroller : public unoRev3API1::ArduinoUnoRev3Class1 {

	/**
	 * Declaration of public (callable from outside the class scope) and private
	 * class members.
	 */
public:
	/**
	 * Declaration of the default-constructor. It is a special member of the
	 * class that has no type and the identifier is equal to the class name.
	 */
	ArduinoMIDIcontroller();

 	/**
 	 * Read potentiometers by measuring the potential between the analog input
 	 * pins and common (GND), smooth potentiometer value using exponential
 	 * moving average as lowpass filter, map to MIDI controller values (0-127) and
 	 * if changed, send MIDI message via USB serial connection. Use the freeware
 	 * virtual MIDI device LoopBe1 (http://www.nerds.de/en/loopbe1.html) as the
 	 * MIDI-serial bridge.
 	 * @param: int8_t potIndex
 	 * @param: uint8_t emaFilterWeight: FilterWeight for the moving average filter
 	 * (default: 2)
 	 * @returns: void (sends MIDI controller values as uint8_t (0-127).
 	 * @fn: Reads potentiometers by measuring the potential between the analog
 	 * input pins and common (GND), smoothing the potentiometer value using the
 	 * exponential moving average as lowpass filter, mapping to MIDI controller
 	 * values (0-127) and send changed controller values as MIDI message via 
	 * USB serial connection.
 	 */
	void sendChangedControllerMIDI(int8_t /* potIndex */,
			uint8_t emaFilterWeight = (uint8_t) 2);
   	int8_t getPotCount();

private:
   	int m_controlChange;
	static constexpr int m_potCount = 6;
	int8_t m_midiCCselects[m_potCount];
	int m_potVals[m_potCount];
	uint8_t m_controllerVals[m_potCount];
	uint8_t m_controllerValsPrevious[m_potCount];
	unoRev3API1::ArduinoUnoRev3Class1 m_MIDIcontrollerArduinoUno;
};


//Do not add code below this line
#endif /* MIDIcontrollerByDerAndere_H */
