/**
 * @title: MIDIbyDerAndere.cpp
 * @version: 1.0
 * @created: 2018
 * @author: DerAndere
 * SPDX-License-Identifier: MIT
 * Copyright DerAndere 2018
 * @license: MIT
 * @info: http://www.it-by-derandere.blogspot.com
 * @language: C++
 * @description: Provides function sendMIDI()
 */
#include <Arduino.h>
#include "MIDIbyDerAndere.h"


// Function for sending a MIDI command:
void sendMIDI(byte statusByte, byte dataByte1, byte dataByte2)
{
  Serial.write(statusByte);
  Serial.write(dataByte1);
  Serial.write(dataByte2);
}
