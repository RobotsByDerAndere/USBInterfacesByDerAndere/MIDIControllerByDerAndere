/**
 * @title: MIDIByDerAndere.h
 * @version: 1.0
 * @created: 2018
 * @author: DerAndere
 * SPDX-License-Identifier: MIT
 * Copyright DerAndere 2018
 * @license: MIT
 * @info: http://www.it-by-derandere.blogspot.com
 * @language: C++
 * @description: Provides function sendMIDI()
 */
#ifndef MIDIbyDerAndere_MIDIbyDerAndere_H
#define MIDIbyDerAndere_MIDIbyDerAndere_H

#include <Arduino.h>

// Function for sending a MIDI command:
void sendMIDI(uint8_t /* statusByte */, uint8_t /* dataByte1 */,
		uint8_t /* dataByte2 */);

#endif /* MIDIbyDerAndere_MIDIbyDerAndere_H */
