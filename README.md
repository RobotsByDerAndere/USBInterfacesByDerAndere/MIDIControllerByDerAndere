# MIDIControllerByDerAndere

MIDIControllerByDerAndere is a MIDI controller based on the Arduino Uno Rev.3
USB development board. 
The GitLab subgroup 
https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/ 
contains the Project MIDIControllerByDerAndere which contains:
A) 	The Firmware-Sketch for the Microchip Atmega329P microcontroller of the
Arduino Uno of the MIDIControllerByDerAndere
B)  A schematic circuit diagram of the hardware

## Dependencies
- Arduino core (https://github.com/arduino/ArduinoCore-avr)
- ArduUnoRev3API1 (https://gitlab.com/RobotsByDerAndere/LibrariesByDerAndereForDevelopmentboards/ArduUnoRev3API1)
- NuMap (https://gitlab.com/RobotsByDerAndere/LibrariesByDerAndereForDevelopmentboards/NuMap)

## Using MIDIControllerByDerAndere
- Make the external libraries listed in section "dependencies" above available to the compiler
- Build the firmware
- Upload the firmware to the Arduino Uno
- Install loopBe1 (http://www.nerds.de/en/loopbe1.html)
- Use loopBe1 as virtual USB to MIDI bridge for the MIDI controller

## Copyright notice

Copyright (c) 2018 DerAndere

This software, excluding third-party software components, is licensed under 
the terms of the MIT License (https://opensource.org/licenses/MIT), 
see ./LICENSE. Contents of the directories ./documentation/ 
and ./src/[Project name]/resources is dual-licensed under the terms of the MIT 
License or the Creative Commons Attribution 4.0 license (CC BY 4.0): 
https://creativecommons.org/licenses/by/4.0/legalcode, see 
./documentation/LICENSE. For all third-party components incorporated into 
this Software, those components are licensed under the original license 
provided by the owner of the applicable component (see ./LICENSE). Source 
code of this software is linked with third-party libraries that are 
licensed under the original license provided by the owner of the applicable 
library (see ./LICENSE and comments in the 
source code). If applicable, third-party components are kept in separate 
child directories ./src/[Project name]/dep/[component name]. Please see the 
file ./src/[Project name]/dep/[component name]/LICENSE file (and the file
./src/[Project name]/dep/[component name]/NOTICE file), if provided, in each 
third-party folder, or otherwise see comments in the source code.

// SPDX-License-Identifier: MIT
